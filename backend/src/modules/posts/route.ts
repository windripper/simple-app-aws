import { FastifyInstance } from 'fastify';
import { pg } from '../../db';

export const postsRoutes = async(server: FastifyInstance) => {
  server.get(
    '/',
    {},
    async(request, reply) => {
      const result = await pg('posts').select('id', 'post');

      reply.status(200).send({ posts: result });
    },
  );

  server.post(
    '/',
    {
      schema: {
        body: {
          type: 'object',
          properties: {
            post: { type: 'string', minLength: 1, maxLength: 255 },
          },
          required: ['post'],
        },
        response: {
          201: { success: true },
        },
      },
    },
    async(request, reply) => {
      await pg.insert(request.body).into('posts');

      reply.status(200).send({ success: true });
    },
  );
};
