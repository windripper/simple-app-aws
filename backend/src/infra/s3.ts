import { S3Client, ListBucketsCommand, HeadBucketCommand, GetObjectCommand } from '@aws-sdk/client-s3';
import { config } from '../config';

const s3 = new S3Client({
  region: config.s3.region,
  credentials: {
    accessKeyId: config.s3.accessKeyId as string,
    secretAccessKey: config.s3.secretAccessKey as string,
  },
});

const Bucket = config.s3.bucketName as string;

export const checkS3Connection = async() => {
  try {
    const listBuckets = new ListBucketsCommand({});
    const headBucket = new HeadBucketCommand({ Bucket });
    await s3.send(listBuckets);
    console.log('[OK] Application connected to S3');
    await s3.send(headBucket);
    console.log(`[OK] Bucket "${Bucket}" is done`);
  } catch (err) {
    console.error(err);
    return new Error('[FAILED] Unable to establish connection to S3');
  }
};

export const Actions = {
  get(Key: string) {
    const getObject = new GetObjectCommand({ Bucket, Key });

    return s3.send(getObject);
  },
};


