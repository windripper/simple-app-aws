import { Knex, knex } from 'knex';
import { config } from '../config';

export const dbConfig: Knex.Config = config.db;

export const pg = knex(dbConfig);

export const checkDBConnection = async() => {
  try {
    await pg.raw('SELECT CURRENT_TIMESTAMP');
    console.log('[OK] Application connected to DB');
  } catch (err) {
    console.error(err);
    throw new Error('Failed to connect to database');
  }
};
