import buildServer from './server';
import { config } from './config';

const main = async() => {
  try {
    const server = await buildServer();

    server.listen({ port: config.port, host: config.host }, (err, address) => {
      if (err) {
        console.error(err);
        process.exit(1);
      }

      console.log(`Server listening at ${address}`);
    });
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
};

main();

process.on('uncaughtException', console.log);
