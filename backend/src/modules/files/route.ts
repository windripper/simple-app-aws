import { FastifyInstance } from 'fastify';
import { Actions } from '../../infra/s3';
import { Readable } from 'stream';
import { FastifyReply } from 'fastify/types/reply';

export const filesRoutes = async(server: FastifyInstance) => {
  server.get(
    '/',
    {},
    async(request, reply: FastifyReply) => {
      const fileName = 'file.txt';
      const file = await Actions.get(fileName);
      const stream = file.Body as Readable;

      await reply.status(200).send(stream);
    },
  );

  server.post(
    '/upload-file',
    {},
    async() => {
      throw new Error('It\'s very serious');
    },
  );
};
