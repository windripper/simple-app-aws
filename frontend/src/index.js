const BASE_URL = import.meta.env.PROD ? 'http://localhost:3000' : 'http://simple-app-alb-644455236.eu-north-1.elb.amazonaws.com';
const postsContainer = document.querySelector('.posts-list');
const addPostBtn = document.querySelector('.add-post-btn');
const addPostInput = document.querySelector('#add-post');
const generateErrorBtn = document.querySelector('.error-btn');
const downloadBtn = document.querySelector('#download-btn');

fetch(`${BASE_URL}/api/posts`)
  .then(res => res.json())
  .then(({ posts = [] }) => {
    posts.forEach(({ id, post }) => {
      const newPostElement = document.createElement('li');
      newPostElement.classList.add('posts-list-item');
      newPostElement.textContent = `${id}. ${post}`;
      postsContainer.appendChild(newPostElement);
    });
  })
  .catch(console.log);

downloadBtn.addEventListener('click', () => {
  fetch(`${BASE_URL}/api/files`)
    .then(response => response.blob())
    .then(blob => {
      const url = URL.createObjectURL(blob);
      const downloadLink = document.createElement('a');
      downloadLink.href = url;
      downloadLink.download = 'filename';
      downloadLink.style.display = 'none';

      document.getElementById('download-link-container').appendChild(downloadLink);
      downloadLink.click();
      URL.revokeObjectURL(url);
      downloadLink.remove();
    })
    .catch(error => {
      console.error('Error downloading the file:', error);
    });
});

addPostBtn.addEventListener('click', () => {
  const { value: post } = addPostInput;

  if (!post) {
    alert('Post should not be empty');
    return;
  }

  fetch(`${BASE_URL}/api/posts`, {
    method: 'POST',
    body: JSON.stringify({ post }),
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then(() => addPostInput.value = '')
    .catch(console.log);
});

generateErrorBtn.addEventListener('click', () => {
  fetch(`${BASE_URL}/api/files/upload-file`, { method: 'POST' })
    .then(res => res.json())
    .then(({ error, message }) => error ? alert(message || 'Error') : null)
    .catch(console.log);
});