import Fastify from 'fastify';
import cors from '@fastify/cors';
import { postsRoutes } from './modules/posts/route';
import { filesRoutes } from './modules/files/route';
import { checkS3Connection } from './infra/s3';
import { checkDBConnection } from './db';
import { checkCloudWatchConnection, logError } from './infra/cloudWatch';
import { config } from './config';

const buildServer = async() => {
  const server = Fastify();

  try {
    await checkDBConnection();
    await checkS3Connection();

    if (config.isProd) {
      await checkCloudWatchConnection();
    }
  } catch (err) {
    if (err instanceof Error) {
      console.error(err.message);
    }

    console.error('Failed to connect to one of the required services');
    process.exit(1);
  }

  server.setErrorHandler((error, request, reply) => {
    if (config.isProd) {
      logError(error);
    } else {
      console.error(error);
    }

    reply.send(error);
  });

  server.register(postsRoutes, { prefix: 'api/posts' });
  server.register(filesRoutes, { prefix: 'api/files' });

  // todo helmet, CORS
  await server.register(cors, {
    origin: '*',
  });

  return server;
};

export default buildServer;