import 'dotenv/config';

const DEFAULT_PORT = 3000;
const DEFAULT_HOST = '0.0.0.0'; // without this fastify not gonna work inside a docker container
const DEV = 'development';
const PROD = 'production';
const ENVIRONMENTS = [DEV, PROD];

if (!process.env.ENV) {
  console.error('No ENV file provided');
  process.exit(1);
}

if (!ENVIRONMENTS.includes(process.env.ENV)) {
  console.error(`Wrong ENV ${process.env.ENV} provided`);
  process.exit(1);
}

export const config = {
  env: process.env.ENV,
  isDev: process.env.ENV === DEV,
  isProd: process.env.ENV === PROD,
  port: process.env.PORT ? parseInt(process.env.PORT) : DEFAULT_PORT,
  host: process.env.HOST || DEFAULT_HOST,
  db: {
    client: 'pg',
    connection: {
      host: process.env.DB_HOST as string,
      port: process.env.DB_PORT as string,
      user: process.env.DB_USER as string,
      password: process.env.DB_PASSWORD as string,
      database: process.env.DB_NAME as string,
      ssl: process.env.ENV === PROD,
    },
    acquireConnectionTimeout: 10000,
  },
  s3: {
    bucketName: process.env.S3_BUCKET_NAME,
    bucketCommon: process.env.S3_BUCKET_COMMON,
    region: process.env.S3_REGION,
    accessKeyId: process.env.S3_ACCESS_KEY_ID,
    secretAccessKey: process.env.S3_SECRET_ACCESS_KEY,
  },
  cloudWatch: {
    region: process.env.CLOUD_WATCH_REGION,
    accessKeyId: process.env.CLOUD_WATCH_ACCESS_KEY_ID,
    secretAccessKey: process.env.CLOUD_WATCH_SECRET_ACCESS_KEY,
    logGroupName: process.env.LOG_GROUP_NAME,
    logStreamName: new Date(new Date().toISOString()).getTime().toString(),
  },
};
