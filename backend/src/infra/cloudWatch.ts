import { CloudWatchLogsClient, CreateLogStreamCommand, PutLogEventsCommand } from '@aws-sdk/client-cloudwatch-logs';
import { config } from '../config';

const client = new CloudWatchLogsClient({
  region: config.cloudWatch.region,
  credentials: {
    accessKeyId: config.cloudWatch.accessKeyId as string,
    secretAccessKey: config.cloudWatch.secretAccessKey as string,
  },
});

const baseInput = {
  logGroupName: config.cloudWatch.logGroupName,
  logStreamName: config.cloudWatch.logStreamName,
};

const prepareStackTrace = (err: unknown) => {
  if (err instanceof Error) {
    return err.message + '\n' + err.stack;
  }

  if (typeof err === 'string') {
    const error = new Error(err);

    return error.message + '\n' + error.stack;
  }

  return new Error().stack;
};

export const logError = async(err: unknown) => {
  const message = prepareStackTrace(err);

  const input = {
    ...baseInput,
    logEvents: [
      {
        timestamp: new Date(new Date().toISOString()).getTime(),
        message,
      },
    ],
  };

  const command = new PutLogEventsCommand(input);
  await client.send(command);
};

export const checkCloudWatchConnection = async() => {
  const command = new CreateLogStreamCommand(baseInput);
  await client.send(command);
  console.log('[OK] Application connected to Cloud Watch');
};